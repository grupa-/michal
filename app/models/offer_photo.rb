class OfferPhoto < ActiveRecord::Base
	mount_uploader :photo, PictureUploader
	belongs_to :offer
end