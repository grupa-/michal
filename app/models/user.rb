class User < ActiveRecord::Base
  mount_uploader :avatar, PictureUploader
  acts_as_commontator
  has_many :events
  has_many :offers
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  after_save :send_welcome_email

private
  def send_welcome_email
    WelcomeMailer.welcome(self).deliver
  end
end