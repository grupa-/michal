class OfferPhotosController < ApplicationController
  before_action :set_offer_photo, only: [:show, :edit, :update, :destroy]

  # GET /offer_photos
  # GET /offer_photos.json
  def index
    @offer_photos = OfferPhoto.all
  end

  # GET /offer_photos/1
  # GET /offer_photos/1.json
  def show
  end

  # GET /offer_photos/new
  def new
    @offer_photo = OfferPhoto.new
  end

  # GET /offer_photos/1/edit
  def edit
  end

  # POST /offer_photos
  # POST /offer_photos.json
  def create
    @offer_photo = OfferPhoto.new(offer_photo_params)

    respond_to do |format|
      if @offer_photo.save
        format.html { redirect_to @offer_photo, notice: 'Offer photo was successfully created.' }
        format.json { render :show, status: :created, location: @offer_photo }
      else
        format.html { render :new }
        format.json { render json: @offer_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /offer_photos/1
  # PATCH/PUT /offer_photos/1.json
  def update
    respond_to do |format|
      if @offer_photo.update(offer_photo_params)
        format.html { redirect_to @offer_photo, notice: 'Offer photo was successfully updated.' }
        format.json { render :show, status: :ok, location: @offer_photo }
      else
        format.html { render :edit }
        format.json { render json: @offer_photo.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /offer_photos/1
  # DELETE /offer_photos/1.json
  def destroy
    @offer_photo.destroy
    respond_to do |format|
      format.html { redirect_to offer_photos_url, notice: 'Offer photo was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_offer_photo
      @offer_photo = OfferPhoto.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def offer_photo_params
      params.require(:offer_photo).permit(:offer_id, :photo)
    end
end
