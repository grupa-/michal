FlatRent::Application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)  
  resources :offer_photos

  resources :photos

  resources :shouts


  resources :offers

  get 'users/ranking'
  resources :events do
    collection do
         get :likeit
     end
  end

#  authenticated do
    root :to => 'home#create', as: :authenticated
#  end
#  devise_scope :user do
#    root to: "devise/sessions#new"
#  end
  devise_for :users, :controllers => {:registrations => "registrations"}
  resources :users

  mount Commontator::Engine => '/commontator'
end