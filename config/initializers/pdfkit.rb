PDFKit.configure do |config|

  config.wkhtmltopdf = '/home/rafal/.rvm/gems/ruby-2.1.1/bin/wkhtmltopdf' if Rails.env.production?
  config.default_options = {
    :page_size => 'Legal',
    :print_media_type => true
  }
  #config.root_url = "http://localhost" 
end